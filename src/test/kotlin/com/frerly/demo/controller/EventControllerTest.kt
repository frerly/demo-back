package com.frerly.demo.controller


import com.fasterxml.jackson.databind.ObjectMapper
import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.FriendDTO
import com.frerly.demo.dto.query.EventSearch
import com.google.gson.Gson
import com.jayway.jsonpath.internal.filter.ValueNodes.JsonNode
import net.datafaker.Faker
import org.junit.jupiter.api.Assertions

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertTimeoutPreemptively
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.UUID


@SpringBootTest
@AutoConfigureMockMvc
class EventControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    var faker = Faker()
    var gson = Gson()

    @Test
    fun `Should create a event` () {
        var friend = FriendDTO()
        friend.fullname = faker.name().fullName()

        var friendId=mockMvc.perform(
            post("/friend")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(friend))
        ).andReturn().response.getHeader("Location").toString().substringAfterLast("/")

        var event = EventDTO()
        event.name = faker.beer().name()
        event.bill = BigDecimal(faker.number().randomDouble(2,10,100)).setScale(2,RoundingMode.HALF_UP)
        event.friendId = friendId

        var resultCreate = mockMvc.perform(
            post("/event")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(event))
        )

        resultCreate
                .andExpect(status().isCreated)
            .andExpect(MockMvcResultMatchers.header().exists("Location"))

        var id=resultCreate.andReturn().response.getHeader("Location").toString()

        mockMvc.perform(get(id))
            .andExpect(status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(event.name))
            .andExpect(MockMvcResultMatchers.jsonPath("$.bill").value(event.bill))
            .andExpect(MockMvcResultMatchers.jsonPath("$.friendId").value(event.friendId))

    }

    @Test
    fun `Should querying events` () {
        mockMvc.perform(
            get("/event/search?start=0&length=5")
                .contentType(MediaType.APPLICATION_JSON)
                ///.content(gson.toJson(eventSearch))
        )
            .andExpect(status().isOk)
            //.andExpect(MockMvcResultMatchers.jsonPath("$.total").value(3))


    }

    @Test
    fun `Should querying debts` () {
        mockMvc.perform(
            get("/event/debts")
                .contentType(MediaType.APPLICATION_JSON)
            ///.content(gson.toJson(eventSearch))
        )
            .andExpect(status().isOk)
        //.andExpect(MockMvcResultMatchers.jsonPath("$.total").value(3))


    }

}