package com.frerly.demo.controller


import com.fasterxml.jackson.databind.ObjectMapper
import com.frerly.demo.dto.FriendDTO
import com.google.gson.Gson
import com.jayway.jsonpath.internal.filter.ValueNodes.JsonNode
import net.datafaker.Faker
import org.junit.jupiter.api.Assertions

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertTimeoutPreemptively
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.UUID


@SpringBootTest
@AutoConfigureMockMvc
class FriendControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    var faker = Faker()
    var gson = Gson()

    @Test
    fun `Should create a friend` () {

        var friend = FriendDTO()
        friend.fullname = faker.name().fullName()


        var resultCreate = mockMvc.perform(
            post("/friend")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(friend))
            )

        resultCreate
            .andExpect(status().isCreated)
            .andExpect(MockMvcResultMatchers.header().exists("Location"))

        var id=resultCreate.andReturn().response.getHeader("Location").toString()

        mockMvc.perform(get(id))
            .andExpect(status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.fullname").value(friend.fullname))
    }

    @Test
    fun `shouldn't find a friend` () {
    mockMvc.perform(get("/friend/"+UUID.randomUUID().toString()))
            .andExpect(status().isNoContent)

    }

    @Test
    fun `You should not register a friend by null` () {
        var friend = FriendDTO()
        mockMvc.perform(
            post("/friend")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(friend))
        ).andExpect(status().isBadRequest)

    }

    @Test
    fun `You should not register a friend by empty name` () {
        var friend = FriendDTO()
        friend.fullname = ""
        mockMvc.perform(
            post("/friend")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(friend))
        ).andExpect(status().isBadRequest)

    }

    @Test
    fun `Should querying debts` () {
        mockMvc.perform(
            get("/friend/all")
                .contentType(MediaType.APPLICATION_JSON)
            ///.content(gson.toJson(eventSearch))
        )
            .andExpect(status().isOk)
        //.andExpect(MockMvcResultMatchers.jsonPath("$.total").value(3))


    }

}