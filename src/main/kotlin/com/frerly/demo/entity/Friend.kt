package com.frerly.demo.entity

import com.frerly.demo.entity.util.AbstractEntity
import jakarta.persistence.Entity
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Getter
import lombok.Setter

@Entity
class Friend : AbstractEntity {

    var fullname:String?=null

    constructor() : super()
    constructor(id: String?) : super() {
        this.id = id
    }
}