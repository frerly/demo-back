package com.frerly.demo.entity

import com.frerly.demo.entity.util.AbstractEntity
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.validation.constraints.NotNull
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Getter
import lombok.Setter
import java.math.BigDecimal

@Entity
class EventFriend : AbstractEntity {

    @JoinColumn(name = "eventId", referencedColumnName = "id", nullable = false, insertable = true, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @NotNull
    var event: Event ? = null

    @JoinColumn(name = "friendId", referencedColumnName = "id", nullable = false, insertable = true, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @NotNull
    var friend: Friend ? = null

    var amount : BigDecimal ? = null

    constructor() : super()
    constructor(id: String?) : super() {
        this.id = id
    }
}