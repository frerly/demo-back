package com.frerly.demo.entity.util

import jakarta.persistence.Id
import jakarta.persistence.MappedSuperclass
import lombok.Getter
import lombok.Setter
import java.io.Serializable

@MappedSuperclass
@Getter
@Setter
abstract class AbstractEntityID : Serializable {
    @Id
    var id: String? = null

    constructor() : super()
    constructor(id: String?) : super() {
        this.id = id
    }
}
