package com.frerly.demo.entity.util

import jakarta.persistence.EntityListeners
import jakarta.persistence.MappedSuperclass
import jakarta.persistence.Temporal
import jakarta.persistence.TemporalType
import jakarta.validation.constraints.NotNull
import lombok.Getter
import lombok.Setter
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.util.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractEntity : AbstractEntityID, Serializable {
    var enabled: @NotNull Boolean = true

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    var createdDate: Calendar? = null

    constructor(id: String?) : super(id)
    constructor() : super()
}
