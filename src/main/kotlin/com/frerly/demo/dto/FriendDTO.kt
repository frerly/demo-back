package com.frerly.demo.dto

import com.frerly.demo.dto.util.AbstractDTO
import com.frerly.demo.entity.Friend
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull

class FriendDTO : AbstractDTO{

    @NotNull
    @NotEmpty
    var fullname:String?=null

    constructor() : super()
    constructor(id: String?) : super(id)
    constructor(friend:Friend){
        id=friend.id
        fullname=friend.fullname
    }
}