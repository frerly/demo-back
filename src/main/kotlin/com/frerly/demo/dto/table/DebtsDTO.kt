package com.frerly.demo.dto.table

import java.math.BigDecimal

interface DebtsDTO {
    var friendId:String?
    var friendName:String?
    var friendDebtId:String?
    var friendDebtName:String?
    var amount: BigDecimal?
}