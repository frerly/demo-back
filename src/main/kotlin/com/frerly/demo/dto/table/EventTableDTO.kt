package com.frerly.demo.dto.table

import com.frerly.demo.dto.EventDTO
import java.math.BigDecimal

class EventTableDTO: EventDTO {
    var friendFullname:String?=null

    constructor(_id:String,_name:String,_bill:BigDecimal,_friendId:String,_friendFullname:String) {
        id=_id
        name=_name
        bill=_bill
        friendId=_friendId
        friendFullname=_friendFullname

    }
}