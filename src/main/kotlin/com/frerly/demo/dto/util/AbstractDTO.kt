package com.frerly.demo.dto.util

import jakarta.persistence.MappedSuperclass
import jakarta.persistence.Temporal
import jakarta.persistence.TemporalType
import jakarta.validation.constraints.NotNull
import lombok.Getter
import lombok.Setter
import org.springframework.data.annotation.CreatedDate
import java.io.Serializable
import java.util.*

abstract class AbstractDTO : AbstractDTOID, Serializable {
    var enabled: Boolean = true

    var createdDate: Calendar? = null

    constructor(id: String?) : super(id)
    constructor() : super()
}
