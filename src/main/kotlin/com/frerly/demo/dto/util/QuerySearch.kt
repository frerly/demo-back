package com.frerly.demo.dto.util

open class QuerySearch(var start: Int = 0, var length:Int = 0, var order: Order = Order())