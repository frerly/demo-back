package com.frerly.demo.dto.util

import jakarta.persistence.Id
import jakarta.persistence.MappedSuperclass
import lombok.Getter
import lombok.Setter
import java.io.Serializable


abstract class AbstractDTOID : Serializable {

    var id: String? = null

    constructor() : super()
    constructor(id: String?) : super() {
        this.id = id
    }
}
