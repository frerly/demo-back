package com.frerly.demo.dto.query

import com.frerly.demo.dto.util.Order
import com.frerly.demo.dto.util.QuerySearch

class EventSearch(start: Int = 0, length: Int = 0, order: Order = Order()) : QuerySearch(start, length, order)