package com.frerly.demo.dto

import com.frerly.demo.dto.util.AbstractDTO
import com.frerly.demo.entity.Friend
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull
import java.math.BigDecimal

open class EventDTO : AbstractDTO{

    var name : String? = null
    var bill : BigDecimal? = null
    var friendId : String? = null

    constructor() : super()
    constructor(id: String?) : super(id)
}