package com.frerly.demo.controller

import com.frerly.demo.dto.FriendDTO
import com.frerly.demo.service.FriendService
import jakarta.validation.Valid
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.util.*
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType


@RestController
@RequestMapping("/friend")
class FriendController(val friendService: FriendService) {

    @PostMapping
    fun register(@Valid @RequestBody friendDTO: FriendDTO): ResponseEntity<JvmType.Object> {
        var id = UUID.randomUUID().toString()
        try {
            friendDTO.id=id
            friendService.save(friendDTO)
        } catch (error: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
        val location: URI = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(id)
            .toUri()
        return ResponseEntity.status(HttpStatus.CREATED)
            .header(HttpHeaders.LOCATION, location.toString())
            .header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.LOCATION)
            .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.LOCATION)
            .build();
    }

    @GetMapping(path = ["/{id}"], produces = ["application/json"])
    fun register(@PathVariable id:String): ResponseEntity<FriendDTO> {
        var friendDTO = friendService.findById(id)
        if(friendDTO.isEmpty){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
        }
        return ResponseEntity.status(HttpStatus.OK).body(friendDTO.get())//.body(Object())
    }


    @GetMapping(path = ["/all"], produces = ["application/json"])
    fun register(): ResponseEntity<List<FriendDTO>> {
        return ResponseEntity.status(HttpStatus.OK).body(friendService.findAll())//.body(Object())
    }

}