package com.frerly.demo.controller

import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.query.EventSearch
import com.frerly.demo.dto.table.DebtsDTO
import com.frerly.demo.dto.table.EventTableDTO
import com.frerly.demo.dto.util.DatatableDTO
import com.frerly.demo.service.EventService
import jakarta.validation.Valid
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.util.*
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType


@RestController
@RequestMapping("/event")
class EventController(val eventService: EventService) {

    @PostMapping
    fun register(@Valid @RequestBody eventDTO: EventDTO): ResponseEntity<JvmType.Object> {
        var id = UUID.randomUUID().toString()
        try {
            eventDTO.id=id
            eventService.save(eventDTO)
        } catch (error: Exception) {
            error.printStackTrace()
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
        val location: URI = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(id)
            .toUri()
        return ResponseEntity.status(HttpStatus.CREATED)
            .header(HttpHeaders.LOCATION, location.toString())
            .header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.LOCATION)
            .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.LOCATION)
            .build();
    }

    @GetMapping(path = ["/{id}"], produces = ["application/json"])
    fun register(@PathVariable id:String): ResponseEntity<EventDTO> {
        var eventDTO = eventService.findById(id)
        if(eventDTO.isEmpty){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
        }
        return ResponseEntity.status(HttpStatus.OK).body(eventDTO.get())//.body(Object())
    }

    @GetMapping(path = ["/search"], produces = ["application/json"])
    fun search(search: EventSearch?): DatatableDTO<EventTableDTO>? {
        return eventService.search(search)
    }

    @GetMapping(path = ["/debts"], produces = ["application/json"])
    fun search(): List<DebtsDTO>? {
        return eventService.debts()
    }

}