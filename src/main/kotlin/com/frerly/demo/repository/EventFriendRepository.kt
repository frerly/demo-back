package com.frerly.demo.repository

import com.frerly.demo.dto.table.DebtsDTO
import com.frerly.demo.entity.Event
import com.frerly.demo.entity.EventFriend
import com.frerly.demo.entity.Friend
import com.frerly.demo.repository.custom.EventCustomRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface EventFriendRepository : JpaRepository<EventFriend, String> {


}