package com.frerly.demo.repository

import com.frerly.demo.entity.Friend
import org.springframework.data.jpa.repository.JpaRepository

interface FriendRepository: JpaRepository<Friend,String> {
}