package com.frerly.demo.repository.custom

import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.query.EventSearch
import com.frerly.demo.dto.table.EventTableDTO
import com.frerly.demo.dto.util.DatatableDTO
import com.frerly.demo.entity.Event
import jakarta.persistence.EntityManager
import jakarta.persistence.TypedQuery
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import java.util.function.IntFunction
import kotlin.collections.ArrayList


class EventCustomRepositoryImpl(private val em: EntityManager?=null): EventCustomRepository {


    override fun search(search: EventSearch?): DatatableDTO<EventTableDTO>? {
        val cb = em!!.criteriaBuilder

        /** Begin createquery  */
        val cq: CriteriaQuery<EventTableDTO> = cb.createQuery(EventTableDTO::class.java)
        val cqc = cb.createQuery(Long::class.java)
        //val andPredicates: MutableList<Predicate> = ArrayList<Predicate>()
        //val andPredicatesc: MutableList<Predicate> = ArrayList<Predicate>()
        /** End createquery  */
        /** Begin FROM y JOINS  */
        val event: Root<Event> = cq.from(Event::class.java)
        val eventc: Root<Event> = cqc.from(Event::class.java)
        /** End FROM y JOINS  */
        /** Begin Where  */
        /*if (!search.getName().isEmpty()) {
            andPredicates.add(
                cb.or(
                    cb.like(cb.upper(company.get("name")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("description")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("ruc")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("address")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("phone")), cb.upper(cb.parameter(String::class.java, "name")))
                )
            )
            andPredicatesc.add(
                cb.or(
                    cb.like(cb.upper(company.get("name")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("description")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("ruc")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("address")), cb.upper(cb.parameter(String::class.java, "name"))),
                    cb.like(cb.upper(company.get("phone")), cb.upper(cb.parameter(String::class.java, "name")))
                )
            )
        }*/
        //cq.where(andPredicates.stream().toArray(IntFunction<Array<A>> { _Dummy_.__Array__() }))
        //cqc.where(andPredicatesc.stream().toArray(IntFunction<Array<A>> { _Dummy_.__Array__() }))
        /** End Where  */
        /** Begin orderBy  */

        cq.orderBy(cb.desc(event.get<Calendar>("createdDate")))
        /** End orderBy  */
        /*_id:String,_name:String,_bill:BigDecimal,_friendId:String,_friendFullname:String*/
        /** Begin select  */
        cq.select(
            cb.construct(
                EventTableDTO::class.java,
                event.get<Any>("id"),
                event.get<Any>("name"),
                event.get<Any>("bill"),
                event.get<Any>("friend").get<Any>("id"),
                event.get<Any>("friend").get<Any>("fullname")
            )
        )
        cqc.select(cb.coalesce(cb.count(eventc), 0L))
        /** End select  */
        val query: TypedQuery<EventTableDTO> = em.createQuery(cq)
        val queryc = em.createQuery(cqc)

        /*if (!search.getName().isEmpty()) {
            query.setParameter("name", search.getName())
            queryc.setParameter("name", search.getName())
        }*/
        /** Tamano  */
        if (search != null) {
            query.setFirstResult(search.start)
        }else{
            query.setFirstResult(0)
        }
        if (search != null) {
            query.setMaxResults(search.length)
        }

        return DatatableDTO(queryc.singleResult, query.getResultList())
    }
}