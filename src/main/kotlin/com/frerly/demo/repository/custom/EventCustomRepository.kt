package com.frerly.demo.repository.custom

import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.query.EventSearch
import com.frerly.demo.dto.table.EventTableDTO
import com.frerly.demo.dto.util.DatatableDTO

interface EventCustomRepository {
    fun search(search: EventSearch?): DatatableDTO<EventTableDTO>?
}