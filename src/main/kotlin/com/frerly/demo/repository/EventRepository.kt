package com.frerly.demo.repository

import com.frerly.demo.dto.table.DebtsDTO
import com.frerly.demo.entity.Event
import com.frerly.demo.entity.Friend
import com.frerly.demo.repository.custom.EventCustomRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface EventRepository : EventCustomRepository, JpaRepository<Event, String> {

    @Query(
        value = """
            select ef.friend_id as friendId,
            (select fullname from friend f where f.id=ef.friend_id) as friendName,
            e.friend_id as friendDebtId,
            (select fullname from friend f where f.id=e.friend_id)  as friendDebtName,
            sum(ef.amount) as amount
            from event_friend ef  join event e on ef.event_id=e.id 
            where ef.friend_id<>e.friend_id 
            group by ef.friend_id ,e.friend_id""",
        // value = "select ef.friend_id as friendId from event_friend ef ",
        nativeQuery = true
    )
    fun debts(): List<DebtsDTO>?
}