package com.frerly.demo.service

import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.query.EventSearch
import com.frerly.demo.dto.table.DebtsDTO
import com.frerly.demo.dto.table.EventTableDTO

import com.frerly.demo.dto.util.DatatableDTO
import java.util.Optional

interface EventService {
    fun save(eventDTO: EventDTO)
    fun findById(id:String):Optional<EventDTO>
    fun search(search: EventSearch?): DatatableDTO<EventTableDTO>?
    fun debts(): List<DebtsDTO>?
}