package com.frerly.demo.service

import com.frerly.demo.dto.FriendDTO
import java.util.Optional

interface FriendService {
    fun save(friendDTO:FriendDTO)
    fun findById(id:String):Optional<FriendDTO>

    fun findAll():List<FriendDTO>
}