package com.frerly.demo.service.impl

import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.FriendDTO
import com.frerly.demo.dto.query.EventSearch
import com.frerly.demo.dto.util.DatatableDTO
import com.frerly.demo.entity.Friend
import com.frerly.demo.repository.EventFriendRepository
import com.frerly.demo.repository.FriendRepository
import com.frerly.demo.service.FriendService
import org.apache.coyote.Response
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.lang.Exception
import java.util.*
import java.util.stream.Collectors

@Service
class FriendServiceImpl(private  var friendRepository: FriendRepository): FriendService {

    override fun save(friendDTO: FriendDTO){
            var friend = Friend(id=friendDTO.id)
            friend.fullname=friendDTO.fullname
            friendRepository.save(friend)

    }



    override fun findById(id: String): Optional<FriendDTO> {
        var friend = friendRepository.findById(id)
        if(friend.isEmpty){
            return Optional.empty()
        }
        return Optional.of(FriendDTO(friend.get()))
    }

    override fun findAll(): List<FriendDTO> {
        return friendRepository.findAll().stream().map { friend-> FriendDTO(friend)}.collect(Collectors.toList())
    }

}