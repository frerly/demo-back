package com.frerly.demo.service.impl

import com.frerly.demo.dto.EventDTO
import com.frerly.demo.dto.FriendDTO
import com.frerly.demo.dto.query.EventSearch
import com.frerly.demo.dto.table.DebtsDTO
import com.frerly.demo.dto.table.EventTableDTO
import com.frerly.demo.dto.util.DatatableDTO
import com.frerly.demo.entity.Event
import com.frerly.demo.entity.EventFriend
import com.frerly.demo.entity.Friend
import com.frerly.demo.repository.EventFriendRepository
import com.frerly.demo.repository.EventRepository
import com.frerly.demo.repository.FriendRepository
import com.frerly.demo.service.EventService
import com.frerly.demo.service.FriendService
import jakarta.transaction.Transactional
import org.apache.coyote.Response
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

@Service
class EventServiceImpl(
    private var eventRepository: EventRepository,
    private var eventFriendRepository: EventFriendRepository,
    private var friendRepository: FriendRepository
) : EventService {

    @Transactional
    override fun save(eventDTO: EventDTO) {
        var event = Event(id = eventDTO.id)
        event.friend = Friend(eventDTO.friendId)
        event.name = eventDTO.name
        event.bill = eventDTO.bill
        eventRepository.save(event)
        splitAccount(event)
    }

    fun splitAccount(event: Event) {
        var friends = friendRepository.findAll()
        var splitAccount = event.bill?.divide(BigDecimal(friends.size),RoundingMode.HALF_DOWN)

        for (friend in friends) {
            var eventFriend = EventFriend(id = UUID.randomUUID().toString())
            eventFriend.event = event
            eventFriend.friend = friend
            eventFriend.amount = splitAccount
            eventFriendRepository.save(eventFriend)
        }


    }

    override fun findById(id: String): Optional<EventDTO> {
        var event = eventRepository.findById(id)
        if (event.isEmpty) {
            return Optional.empty()
        }
        var eventDTO = EventDTO(id = event.get().id)
        eventDTO.name = event.get().name
        eventDTO.friendId = event.get().friend?.id
        eventDTO.bill = event.get().bill
        return Optional.of(eventDTO)
    }

    override fun search(search: EventSearch?): DatatableDTO<EventTableDTO>?{
        return eventRepository.search(search)
    }

    override fun debts(): List<DebtsDTO>? {
        return eventRepository.debts()
    }

}